﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abatap.DataLayer.Model
{
    public abstract class BaseEntity
    {
        [Required]
        [StringLength(32)]
        public string LastModUser { get; set; }

        public DateTime LastModDate { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Timestamp { get; set; }

        public void SetAuditValues(DateTime created, string createdBy)
        {
            if (LastModDate == DateTime.MinValue) LastModDate = created;
            if (string.IsNullOrEmpty(LastModUser)) LastModUser = createdBy;
        }
    }
}
