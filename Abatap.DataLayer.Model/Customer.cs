using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Abatap.DataLayer.Model
{
    [Table("Customer")]
    public partial class Customer : BaseEntity
    {
        public Customer()
        {
            Account = new HashSet<Account>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string Surname { get; set; }

        [Required]
        [StringLength(11)]
        public string PersonalId { get; set; }

        [StringLength(13)]
        public string Phone { get; set; }

        public virtual ICollection<Account> Account { get; set; }
    }
}
