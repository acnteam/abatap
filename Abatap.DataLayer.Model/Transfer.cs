using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Abatap.DataLayer.Model
{
    [Table("Transfer")]
    public partial class Transfer : BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int AccountFromId { get; set; }

        public int AccountToId { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        [StringLength(3)]
        public string Currency { get; set; }

        public virtual Account AccountFrom { get; set; }

        public virtual Account AccountTo { get; set; }
    }
}
