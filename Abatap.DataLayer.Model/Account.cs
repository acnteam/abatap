using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Abatap.DataLayer.Model
{
    [Table("Account")]
    public partial class Account : BaseEntity
    {
        public Account()
        {
            OutgoingTransfers = new HashSet<Transfer>();
            IncomingTransfers = new HashSet<Transfer>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int CustomerId { get; set; }

        [Required]
        public decimal Balance { get; set; }

        [Required]
        [StringLength(34)]
        public string AccountNumber { get; set; }

        [Required]
        [StringLength(3)]
        public string Currency { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual ICollection<Transfer> OutgoingTransfers { get; set; }

        public virtual ICollection<Transfer> IncomingTransfers { get; set; }
    }
}
