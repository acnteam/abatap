﻿using Abatap.DataLayer.Model;
using Abatap.Services.Contracts.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abatap.AppSrv.Business
{
    public class Automapper
    {
        public static void Configure()
        {
            AutoMapper.Mapper.CreateMap<Transfer,TransferDto>()
                .ForMember(dst=>dst.AccountFrom,opt=>opt.MapFrom(src=>src.AccountFrom))
                 .ForMember(dst=>dst.AccountTo,opt=>opt.MapFrom(src=>src.AccountTo))   ;
        }
    }
}
