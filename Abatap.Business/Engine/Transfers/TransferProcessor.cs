﻿using Abatap.AppSrv.Business.Engine.CurrencyRates.Converter;
using Abatap.DataLayer.Database;
using Abatap.DataLayer.Database.Repository.Repositories;
using Abatap.DataLayer.Model;

namespace Abatap.AppSrv.Business.Engine.Transfers
{
    public class TransferProcessor
    {
        private readonly AbatapDbContext _ctx;
        private readonly Account _sourceAccount;
        private readonly Account _destinationAccount;
        private readonly decimal _amount;
        private readonly string _currency;

        public TransferProcessor(
            AbatapDbContext ctx,
            Account sourceAccount, 
            Account destinationAccount,
            decimal amount,
            string currency
            )
        {
            _ctx = ctx;
            _sourceAccount = sourceAccount;
            _destinationAccount = destinationAccount;
            _amount = amount;
            _currency = currency;
        }

        public TransferValidationResult Validate()
        {
            var result = new TransferValidationResult();

            return result;
        }

        public void Execute()
        {
            SaveTransferRecord();
            UpdateSourceAccountBalance();
            UpdateDestinationAccountBalance();
        }

        private void SaveTransferRecord()
        {
            var transfer = new Transfer()
            {
                AccountFromId = _sourceAccount.Id,
                AccountToId = _destinationAccount.Id,
                Amount = _amount,
                Currency = _currency
            };
            var transferRepository = new TransferRepository(_ctx);
            transferRepository.Save(transfer);
        }

        private void UpdateSourceAccountBalance()
        {
            decimal amountInSourceAccountCurrency;
            if (_currency == _sourceAccount.Currency)
            {
                amountInSourceAccountCurrency = _amount;
            }
            else
            {
                decimal transferAmountInPln;
                if (_currency != CurrencyConverter.Pln)
                    transferAmountInPln = CurrencyConverter.Instance.ConvertToPln(_currency, _amount);
                else
                    transferAmountInPln = _amount;

                if (_sourceAccount.Currency != CurrencyConverter.Pln)
                    amountInSourceAccountCurrency = CurrencyConverter.Instance.ConvertFromPln(_currency, transferAmountInPln);
                else
                    amountInSourceAccountCurrency = transferAmountInPln;
            }
            _sourceAccount.Balance -= amountInSourceAccountCurrency;
        }

        private void UpdateDestinationAccountBalance()
        {
            decimal amountInDestinationAccountCurrency;
            if (_currency == _destinationAccount.Currency)
            {
                amountInDestinationAccountCurrency = _amount;
            }
            else
            {
                decimal transferAmountInPln;
                if (_currency != CurrencyConverter.Pln)
                    transferAmountInPln = CurrencyConverter.Instance.ConvertToPln(_currency, _amount);
                else
                    transferAmountInPln = _amount;

                if (_destinationAccount.Currency != CurrencyConverter.Pln)
                    amountInDestinationAccountCurrency = CurrencyConverter.Instance.ConvertFromPln(_currency, transferAmountInPln);
                else
                    amountInDestinationAccountCurrency = transferAmountInPln;
            }
            _destinationAccount.Balance += amountInDestinationAccountCurrency;
        }
    }
}
