﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abatap.AppSrv.Business.Engine.Transfers
{
    public class TransferValidationResult
    {
        public string Message { get; set; }
        public bool IsValid { get; set; }
    }
}
