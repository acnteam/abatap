﻿namespace Abatap.AppSrv.Business.Engine.CurrencyRates
{
    /// <summary>
    /// Kurs waluty
    /// </summary>
    public struct CurrencyRate
    {
        public string CurrencyCode;
        public decimal Rate;
    }
}
