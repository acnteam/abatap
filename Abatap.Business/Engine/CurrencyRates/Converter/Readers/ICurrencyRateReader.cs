﻿using System.Collections.Generic;

namespace Abatap.AppSrv.Business.Engine.CurrencyRates.Converter.Readers
{
    public interface ICurrencyRateReader
    {
        List<CurrencyRate> GetCurrencyRates();
    }
}
