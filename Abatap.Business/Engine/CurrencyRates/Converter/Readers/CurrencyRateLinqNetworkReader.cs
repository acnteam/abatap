﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using Abatap.AppSrv.Business.Engine.CurrencyRates.Converter.Readers;

namespace Abatap.AppSrv.Business.Engine.Currency.Readers
{
    public class CurrencyRateLinqReader : AbstractEbcCurrencyRateReader
    {
        private const string EbcRatesUrl = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

        protected override XDocument GetRatesFile()
        {
            var webClient = new WebClient();
            String ratesText = webClient.DownloadString(EbcRatesUrl);

            return XDocument.Load(new XmlTextReader(ratesText));
        }
    }
}
