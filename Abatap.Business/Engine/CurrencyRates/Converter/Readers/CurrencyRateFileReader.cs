﻿using System.Xml.Linq;
using Abatap.AppSrv.Business.Engine.Currency.Readers;

namespace Abatap.AppSrv.Business.Engine.CurrencyRates.Converter.Readers
{
    public class CurrencyRateFileReader : AbstractEbcCurrencyRateReader
    {
        protected override XDocument GetRatesFile()
        {
            return XDocument.Load("CurrencyRates.xml");
        }
    }
}
