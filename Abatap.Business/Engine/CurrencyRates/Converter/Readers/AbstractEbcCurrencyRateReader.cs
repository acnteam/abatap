﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace Abatap.AppSrv.Business.Engine.CurrencyRates.Converter.Readers
{
    public abstract class AbstractEbcCurrencyRateReader : ICurrencyRateReader
    {
        protected abstract XDocument GetRatesFile();

        public List<CurrencyRate> GetCurrencyRates()
        {
            XDocument doc = GetRatesFile();

            var currencyRates = new List<CurrencyRate>();
            if (doc.Document != null)
            {
                XNamespace Namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref";

                XElement cube = doc.Document.Descendants(Namespace + "Cube").First();
                XElement cubeWithDate = cube.Elements().First();

                foreach (XElement element in cubeWithDate.Elements())
                {
                    string rateString = element.Attribute("rate").Value;
                    decimal rate = decimal.Parse(rateString, new CultureInfo("En"));
                    string currency = element.Attribute("currency").Value;

                    var currencyRate = new CurrencyRate { CurrencyCode = currency, Rate = rate };
                    currencyRates.Add(currencyRate);
                }
            }
            return currencyRates;
        }
    }
}
