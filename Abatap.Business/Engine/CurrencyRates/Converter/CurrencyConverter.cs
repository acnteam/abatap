﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abatap.AppSrv.Business.Engine.Currency.Readers;
using Abatap.AppSrv.Business.Engine.CurrencyRates.Converter.Readers;

namespace Abatap.AppSrv.Business.Engine.CurrencyRates.Converter
{
    /// <summary>
    /// Komentarz
    /// </summary>
    public sealed class CurrencyConverter
    {
        public static string Pln = "PLN";

        private static volatile CurrencyConverter _instance;
        private static readonly object SyncRoot = new object();
        private static DateTime _lastRefreshDate;

        private readonly Dictionary<string, decimal> _currencyRates = new Dictionary<string, decimal>();

        private void RefreshCurrencyList()
        {
            ICurrencyRateReader reader = new MockCurrencyRateReader();
            List<CurrencyRate> currencyList = reader.GetCurrencyRates();

            _currencyRates.Clear();
            foreach (var rate in currencyList)
            {
                _currencyRates.Add(rate.CurrencyCode, rate.Rate);
            }
            _lastRefreshDate = DateTime.Now;
        }

        private CurrencyConverter()
        {
            RefreshCurrencyList();
        }

        public static CurrencyConverter Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null || _lastRefreshDate < DateTime.Now.AddDays(-1))
                            _instance = new CurrencyConverter();
                    }
                }

                return _instance;
            }
        }

        public List<String> CurrencyList
        {
            get { return _currencyRates.Keys.ToList(); }
        }

        public decimal ConvertToPln(string currencyCode, decimal amount)
        {
            if (!_currencyRates.ContainsKey(currencyCode))
                throw new Exception("Currency: " + currencyCode + " is not supported.");

            return _currencyRates[currencyCode]*amount;
        }

        public decimal ConvertFromPln(string currencyCode, decimal amount)
        {
            if (!_currencyRates.ContainsKey(currencyCode))
                throw new Exception("Currency: " + currencyCode + " is not supported.");

            return amount / _currencyRates[currencyCode];
        }
    }
}
