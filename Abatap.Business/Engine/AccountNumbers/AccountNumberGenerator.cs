﻿using Abatap.DataLayer.Model;

namespace Abatap.AppSrv.Business.Engine.AccountNumbers
{
    public class AccountNumberGenerator : IAccountNumberGenerator
    {
        public string GenerateAccountNumber(Customer customer, string currency)
        {
            return "000" + customer.Id + currency;
        }
    }
}
