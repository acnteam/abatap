using Abatap.DataLayer.Model;

namespace Abatap.AppSrv.Business.Engine.AccountNumbers
{
    public interface IAccountNumberGenerator
    {
        string GenerateAccountNumber(Customer customer, string currency);
    }
}