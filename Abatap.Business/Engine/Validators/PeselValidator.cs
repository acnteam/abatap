﻿using System;

namespace Abatap.AppSrv.Business.Engine.Validators
{
    public class PeselValidator
    {
        private static readonly int[] Multipliers = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };

        public void ValidatePesel(string pesel)
        {
            if (pesel == null)
                throw new ArgumentException("Pesel nie może być pusty.");

            if (pesel.Length == 11)
            {
                string checksum = CalculateCheckSum(pesel);
                bool isChecksumCorrect = checksum.Equals(pesel[10].ToString());
                if (!isChecksumCorrect)
                    throw new ArgumentException("Suma kontrolna numeru PESEL jest niepoprawna. Jest równa: " + checksum + " a powinna być: " + pesel[10]);
            }
            else
            {
                throw new ArgumentException("Długość numeru PESEL jest niepoprawna. Powinna być równa: 11 a jest równa: " + pesel.Length);
            }
        }

        public string CalculateCheckSum(string pesel)
        {
            int sum = 0;
            for (int i = 0; i < Multipliers.Length; i++)
            {
                int value;
                if (!int.TryParse(pesel[i].ToString(), out value))
                    throw new ArgumentException("Pozycja: " + i + " równa: " + pesel[i] + " w numerze PESEL nie jest poprawną cyfrą.");

                sum += Multipliers[i]*value;
            }
            int ramaining = sum % 10;
            string checkSum = ramaining == 0 ? ramaining.ToString() : (10 - ramaining).ToString();
            return checkSum;
        }
    }
}
