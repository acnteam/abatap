﻿using System;
using System.Collections.Generic;
using Abatap.AppSrv.Business.Engine.Transfers;
using Abatap.DataLayer.Database;
using Abatap.DataLayer.Database.Repository.Repositories;
using Abatap.DataLayer.Model;
using Abatap.Services.Contracts;
using Abatap.Services.Contracts.Model;

namespace Abatap.AppSrv.Business.Services
{
    public class TransferService : ITransferService
    {
        public TransferService()
        {
            Automapper.Configure();
        }

        public IEnumerable<TransferDto> GetTransfersHistory()
        {
            using (var ctx = new AbatapDbContext())
            {
                var transferRepository = new TransferRepository(ctx);
                IEnumerable<Transfer> transfers = transferRepository.FindAll();
                var r= AutoMapper.Mapper.Map<IEnumerable<TransferDto>>(transfers);
                return r;
            }
        }

        public TransferResultDto ExecuteTransfer(TransferRequestDto transferRequest)
        {
            try
            {
                using (var ctx = new AbatapDbContext())
                {
                    var accountRepository = new AccountRepository(ctx);

                    Account sourceAccount = accountRepository.Get(transferRequest.SourceAccountId);
                    Account destinationAccount = accountRepository.GetAccountByNumber(transferRequest.DestinationAccountNumber);

                    var transferProcessor = new TransferProcessor(ctx, sourceAccount, destinationAccount, transferRequest.Amount, transferRequest.Currency);

                    var result = new TransferResultDto();

                    TransferValidationResult validationResult = transferProcessor.Validate();
                    if (!validationResult.IsValid)
                    {
                        result.ResultCode = TransferResultDto.TransferResultCode.Invalid;
                        result.Message = validationResult.Message;
                        return result;
                    }
                    transferProcessor.Execute();
                    ctx.SaveChanges();

                    result.ResultCode = TransferResultDto.TransferResultCode.Success;
                    return result;
                }
            }
            catch
            {
                // TODO: Log error
                var result = new TransferResultDto
                {
                    ResultCode = TransferResultDto.TransferResultCode.Failed
                };
                return result;
            }
        }
    }
}
