﻿using System.Collections.Generic;
using Abatap.AppSrv.Business.Engine.AccountNumbers;
using Abatap.AppSrv.Business.Engine.CurrencyRates.Converter;
using Abatap.DataLayer.Database;
using Abatap.DataLayer.Database.Repository.Repositories;
using Abatap.DataLayer.Model;
using Abatap.Services.Contracts;
using Abatap.Services.Contracts.Model;

namespace Abatap.AppSrv.Business.Services
{
    public class AccountService: IAccountService
    {
        static AccountService()
        {
            AutoMapper.Mapper.CreateMap<Account, AccountDto>();
        }

        public AccountDto GetAccount(string accountNo)
        {
            using (var ctx = new AbatapDbContext())
            {
                var accountRepository = new AccountRepository(ctx);
                Account account = accountRepository.GetAccountByNumber(accountNo);
                return AutoMapper.Mapper.Map<AccountDto>(account);
            }
        }

        public IEnumerable<AccountDto> GetAccounts(int customerId)
        {
            using (var ctx = new AbatapDbContext())
            {
                var accountRepository = new AccountRepository(ctx);
                List<Account> accounts = accountRepository.GetAccountsForCustomer(customerId);
                return AutoMapper.Mapper.Map<List<AccountDto>>(accounts);
            }
        }

        public AccountDto OpenAccount(int customerId, string currency)
        {
            using (var ctx = new AbatapDbContext())
            {
                var customerRepository = new CustomerRepository(ctx);
                Customer customer = customerRepository.Get(customerId);

                var newAccount = new Account();
                IAccountNumberGenerator accountNumberGenerator = new AccountNumberGenerator();
                newAccount.AccountNumber = accountNumberGenerator.GenerateAccountNumber(customer, currency);
                newAccount.Balance = 0;
                newAccount.Currency = currency;
                newAccount.CustomerId = customerId;

                var accountRepository = new AccountRepository(ctx);
                accountRepository.Save(newAccount);

                ctx.SaveChanges();
                return AutoMapper.Mapper.Map<AccountDto>(newAccount);
            }
        }

        public List<string> GetAvailableCurrencies()
        {
            return CurrencyConverter.Instance.CurrencyList;
        }
    }
}
