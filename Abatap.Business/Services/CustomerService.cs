﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Abatap.AppSrv.Business.Engine.Validators;
using Abatap.AppSrv.Business.Exceptions;
using Abatap.DataLayer.Database;
using Abatap.DataLayer.Database.Repository.Repositories;
using Abatap.DataLayer.Model;
using Abatap.Services.Contracts;
using Abatap.Services.Contracts.Exceptions;
using Abatap.Services.Contracts.Model;

namespace Abatap.AppSrv.Business.Services
{
    public class CustomerService : ICustomerService
    {
        static CustomerService()
        {
            AutoMapper.Mapper.CreateMap<Customer, CustomerDto>();
            AutoMapper.Mapper.CreateMap<CustomerDto, Customer>();
        }

        public CustomerDto GetCustomer(int customerId)
        {
            using (var ctx = new AbatapDbContext())
            {
                var customerRepository = new CustomerRepository(ctx);
                Customer cust = customerRepository.Get(customerId);
                return AutoMapper.Mapper.Map<CustomerDto>(cust);
            }
        }

        public IEnumerable<CustomerDto> GetAllCustomers()
        {
            using (var ctx = new AbatapDbContext())
            {
                var customerRepository = new CustomerRepository(ctx);
                IEnumerable<Customer> customers = customerRepository.FindAll();
                return AutoMapper.Mapper.Map<IEnumerable<CustomerDto>>(customers);
            }
        }

        public CustomerDto SaveCustomer(CustomerDto customer)
        {
            using (var ctx = new AbatapDbContext())
            {
                try
                {
                    var peselValidator = new PeselValidator();
                    peselValidator.ValidatePesel(customer.PersonalId);

                    var customerRepository = new CustomerRepository(ctx);
                    var customerToSave = AutoMapper.Mapper.Map<Customer>(customer);
                    customerRepository.Save(customerToSave);
                    ctx.SaveChanges();
                    return AutoMapper.Mapper.Map<CustomerDto>(customerToSave);
                }
                catch (ArgumentException ex)
                {
                    throw new FaultException<AbatapFaultDetails>(new AbatapFaultDetails(false), new FaultReason("Nie można zapisać klienta. Przyczyna: " + ex.Message));
                }
                catch (Exception)
                {
                    throw new FaultException<AbatapFaultDetails>(new AbatapFaultDetails(true), new FaultReason("Wystąpił błąd techniczny podczas zapisu klienta."));
                }
            }
        }

        public void DeleteCustomer(int customerId)
        {
            using (var ctx = new AbatapDbContext())
            {
                var customerRepository = new CustomerRepository(ctx);
                Customer customer = customerRepository.Get(customerId);
                customerRepository.Delete(customer);
                ctx.SaveChanges();
            }
        }
    }
}
