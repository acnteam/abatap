using System;
using System.Data.Entity;
using System.Linq;
using Abatap.DataLayer.Model;

namespace Abatap.DataLayer.Database
{
    public partial class AbatapDbContext : DbContext
    {
        public AbatapDbContext()
            : base("name=AbatapDatabaseConnection")
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Transfer> Transfer { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .Property(e => e.AccountNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .Property(e => e.LastModUser)
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .Property(e => e.Timestamp)
                .IsFixedLength();

            modelBuilder.Entity<Account>()
                .HasMany(e => e.OutgoingTransfers)
                .WithRequired(e => e.AccountFrom)
                .HasForeignKey(e => e.AccountFromId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.IncomingTransfers)
                .WithRequired(e => e.AccountTo)
                .HasForeignKey(e => e.AccountToId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Surname)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.PersonalId)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Phone)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.LastModUser)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Timestamp)
                .IsFixedLength();

            modelBuilder.Entity<Transfer>()
                .Property(e => e.LastModUser)
                .IsUnicode(false);

            modelBuilder.Entity<Transfer>()
                .Property(e => e.Timestamp)
                .IsFixedLength();
        }

        public override int SaveChanges()
        {
            foreach (var entity in ChangeTracker.Entries()
                                                .Where(e => e.State == EntityState.Added)
                                                .Select(e => e.Entity)
                                                .OfType<BaseEntity>())
            {
                entity.SetAuditValues(DateTime.Now, GetCurrentUser());
            }
            return base.SaveChanges();
        }

        internal string GetCurrentUser()
        {
            var windowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent();
            if (windowsIdentity != null)
                return windowsIdentity.Name;
            return String.Empty;
        }
    }
}
