﻿using System.Collections.Generic;
using System.Data.Entity;
using Abatap.DataLayer.Model;

namespace Abatap.DataLayer.Database
{
    public class SchoolInitializer : DropCreateDatabaseIfModelChanges<AbatapDbContext>
    {
        protected override void Seed(AbatapDbContext context)
        {
            var customers = new List<Customer>
            {
                new Customer {Name = "Jan", Surname = "Kowalski", PersonalId = "8301931245", Phone = "432533234"},
                new Customer {Name = "Adam", Surname = "Nowak", PersonalId = "8301931245", Phone = "432533234"}
            };
            customers.ForEach(s => context.Customer.Add(s));
            context.SaveChanges();
            var accounts = new List<Account>
            {
                new Account {CustomerId = 1, AccountNumber = "001001", Balance = 100, Currency = "PLN"},
                new Account {CustomerId = 1, AccountNumber = "001002", Balance = 0, Currency = "PLN"},
                new Account {CustomerId = 1, AccountNumber = "001003", Balance = 0, Currency = "EUR"},
                new Account {CustomerId = 2, AccountNumber = "002003", Balance = 50, Currency = "USD"}
            };
            accounts.ForEach(s => context.Account.Add(s));
            context.SaveChanges();
        }
    }
}