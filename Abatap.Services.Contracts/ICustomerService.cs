﻿using System.Collections.Generic;
using System.ServiceModel;
using Abatap.Services.Contracts.Exceptions;
using Abatap.Services.Contracts.Model;

namespace Abatap.Services.Contracts
{
    [ServiceContract]
    public interface ICustomerService
    {
        [OperationContract]
        CustomerDto GetCustomer(int customerId);

        [OperationContract]
        IEnumerable<CustomerDto> GetAllCustomers();

        [OperationContract]
        [FaultContract(typeof(AbatapFaultDetails))]
        CustomerDto SaveCustomer(CustomerDto customer);

        [OperationContract]
        void DeleteCustomer(int customerId);
    }
}
