﻿using System.Collections.Generic;
using System.ServiceModel;
using Abatap.Services.Contracts.Model;

namespace Abatap.Services.Contracts
{
    [ServiceContract]
    public interface ITransferService
    {
        [OperationContract]
        IEnumerable<TransferDto> GetTransfersHistory();

        [OperationContract]
        TransferResultDto ExecuteTransfer(TransferRequestDto sourceAccountId);
    }
}
