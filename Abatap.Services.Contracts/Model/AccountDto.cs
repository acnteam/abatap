﻿using System.Runtime.Serialization;

namespace Abatap.Services.Contracts.Model
{
    [DataContract]
    public class AccountDto
    {
        [DataMember]
        public string AccountNumber { get; set; }

        [DataMember]
        public string Currency { get; set; }

        [DataMember]
        public double Balance { get; set; }

        [DataMember]
        public int CustomerId { get; set; }
    }
}
