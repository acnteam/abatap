﻿using System.Runtime.Serialization;

namespace Abatap.Services.Contracts.Model
{
    [DataContract]
    public class TransferDto
    {
        [DataMember]
        public double Amount { get; set; }

        [DataMember]
        public string Currency { get; set; }

        [DataMember]
        public string AccountTo { get; set; }

        [DataMember]
        public string AccountFrom { get; set; }
    }
}
