﻿using System.Runtime.Serialization;

namespace Abatap.Services.Contracts.Model
{
    [DataContract]
    public class TransferResultDto
    {
        [DataMember]
        public TransferDto Transfer { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public TransferResultCode ResultCode { get; set; }

        public enum TransferResultCode
        {
            Success, Failed, Invalid
        }
    }
}
