﻿using System.Runtime.Serialization;

namespace Abatap.Services.Contracts.Model
{
    [DataContract]
    public class CustomerDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string PersonalId { get; set; }

        [DataMember]
        public string Phone { get; set; }
    }
}
