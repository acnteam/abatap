﻿using System.Runtime.Serialization;

namespace Abatap.Services.Contracts.Model
{
    [DataContract]
    public class TransferRequestDto
    {
        [DataMember]
        public int SourceAccountId { get; set; }

        [DataMember]
        public string DestinationAccountNumber { get; set; }

        [DataMember]
        public decimal Amount{ get; set; }

        [DataMember]
        public string Currency { get; set; }
    }
}
