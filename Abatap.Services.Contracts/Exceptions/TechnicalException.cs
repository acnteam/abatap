﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abatap.AppSrv.Business.Exceptions
{
    public class TechnicalException : Exception
    {
        public TechnicalException(string message) : base(message)
        {
        }
    }
}
