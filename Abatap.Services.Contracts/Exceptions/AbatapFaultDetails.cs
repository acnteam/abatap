﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Abatap.Services.Contracts.Exceptions
{
    [DataContract]
    public class AbatapFaultDetails
    {
        public AbatapFaultDetails(bool isTechnical)
        {
            IsTechnical = isTechnical;
        }

        [DataMember]
        public bool IsTechnical { get; set; }
    }
}
