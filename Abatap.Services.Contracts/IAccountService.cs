﻿using System.Collections.Generic;
using System.ServiceModel;
using Abatap.Services.Contracts.Model;

namespace Abatap.Services.Contracts
{
    [ServiceContract]
    public interface IAccountService
    {
        [OperationContract]
        AccountDto GetAccount(string accountNo);

        [OperationContract]
        IEnumerable<AccountDto> GetAccounts(int customerId);

        [OperationContract]
        AccountDto OpenAccount(int customerId, string currency);

        [OperationContract]
        List<string> GetAvailableCurrencies();
    }
}