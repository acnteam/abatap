﻿using System;
using Abatap.AppSrv.Business.Engine.AccountNumbers;
using Abatap.AppSrv.Business.Services;
using Abatap.DataLayer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Abatap.Business.Tests
{
    [TestClass]
    public class AccountNumberGeneratorTest
    {
        [TestMethod]
        public void TestAccountNumberGeneration()
        {
            var generator = new AccountNumberGenerator();

            var customer = new Customer()
            {
                Name = "John",
                Surname = "Smith",
                Id = 1
            };
            string number = generator.GenerateAccountNumber(customer, "PLN");

            Assert.AreEqual(number, "0001PLN");
        }
    }
}
