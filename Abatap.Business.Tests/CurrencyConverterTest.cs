﻿using Abatap.AppSrv.Business.Engine.Currency;
using Abatap.AppSrv.Business.Engine.CurrencyRates.Converter;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Abatap.Business.Tests
{
    [TestClass]
    public class CurrencyConverterTest
    {
        [TestMethod]
        public void ConvertEurToPln()
        {
            decimal plnAmount = CurrencyConverter.Instance.ConvertToPln("EUR", 1);
            Assert.AreEqual(plnAmount, 4.3m);
        }
    }
}