﻿using System.Collections.Generic;
using Abatap.Client.ServiceProxies.Customer;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.Tests.Mocks
{
    public class MockCustomerServiceProxy : ICustomerServiceProxy
    {
        public CustomerDto SavedCustomer { get; set; }

        public IEnumerable<CustomerDto> GetAllCustomers()
        {
            throw new System.NotImplementedException();
        }

        public void DeleteCustomer(int customerId)
        {
            throw new System.NotImplementedException();
        }

        public CustomerDto SaveCustomer(CustomerDto customer)
        {
            SavedCustomer = customer;
            return customer;
        }
    }
}
