﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abatap.Client.Presenters.Customers;
using Abatap.Client.Views.Customers;

namespace Abatap.Client.Tests.Mocks
{
    public class MockCustomerDetailsView : ICustomerDetailsView
    {
        public void Dispose()
        {
            
        }

        public event EventHandler Load;
        public event EventHandler Disposed;
        public CustomerDetailsPresenter Presenter { get; set; }
        public string CustomerName { get; set; }
        public string Surname { get; set; }
        public string PersonalId { get; set; }
        public string Phone { get; set; }
    }
}
