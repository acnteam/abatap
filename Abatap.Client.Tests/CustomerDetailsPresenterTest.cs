﻿using System;
using Abatap.Client.Presenters.Customers;
using Abatap.Client.ServiceProxies.Customer;
using Abatap.Client.Tests.Mocks;
using Abatap.Client.Views.Customers;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Abatap.Client.Tests
{
    [TestClass]
    public class CustomerDetailsPresenterTest
    {
        [TestMethod]
        public void SaveCustomerTest()
        {
            var container = new UnityContainer();

            var customerServiceProxy = new MockCustomerServiceProxy();
            var customerDetailsView = new MockCustomerDetailsView();

            container.RegisterInstance<ICustomerServiceProxy>(customerServiceProxy);
            container.RegisterInstance<ICustomerDetailsView>(customerDetailsView);

            container.RegisterType<CustomerDetailsPresenter>();

            var presenter = (CustomerDetailsPresenter)container.Resolve(typeof(CustomerDetailsPresenter));

            customerDetailsView.CustomerName = "John";
            presenter.SaveCustomer();

            Assert.AreEqual(customerServiceProxy.SavedCustomer.Name, "John");
        }
    }
}
