﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Abatap.DataLayer.Model;

namespace Abatap.DataLayer.Database.Repository.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly AbatapDbContext _dbContext;

        public CustomerRepository(AbatapDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Customer> FindAll()
        {
            return _dbContext.Customer.ToList();
        }

        public Customer GetByPersonalId(string personalId)
        {
            return _dbContext.Customer.FirstOrDefault(x => x.PersonalId == personalId);
        }

        public Customer Get(int id)
        {
            return _dbContext.Customer.FirstOrDefault(x => x.Id == id);
        }

        public void Save(Customer entity)
        {
            if (entity.Id > 0)
            {
                _dbContext.Customer.Attach(entity);
                _dbContext.Entry(entity).State = EntityState.Modified;
            }
            else
            {
                _dbContext.Customer.Add(entity);
            }
        }

        public void Delete(Customer entity)
        {
            _dbContext.Customer.Remove(entity);
        }
    }
}
