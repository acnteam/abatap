﻿using System.Collections.Generic;
using Abatap.DataLayer.Model;

namespace Abatap.DataLayer.Database.Repository.Repositories
{
    public interface ICustomerRepository : IRepository<Customer, int>
    {
        IEnumerable<Customer> FindAll();
        Customer GetByPersonalId(string text);
    }
}
