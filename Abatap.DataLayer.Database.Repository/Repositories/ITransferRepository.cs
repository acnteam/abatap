﻿using System.Collections.Generic;
using Abatap.DataLayer.Model;

namespace Abatap.DataLayer.Database.Repository.Repositories
{
    public interface ITransferRepository : IRepository<Transfer, int>
    {
        IEnumerable<Transfer> FindAll();
    }
}
