﻿using System.Collections.Generic;
using Abatap.DataLayer.Model;

namespace Abatap.DataLayer.Database.Repository.Repositories
{
    public interface IAccountRepository : IRepository<Account, int>
    {
        IEnumerable<Account> FindAll();

        Account GetAccountByNumber(string accountNumber);
    }
}
