﻿using System.Collections.Generic;
using System.Linq;
using Abatap.DataLayer.Model;

namespace Abatap.DataLayer.Database.Repository.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly AbatapDbContext _dbContext;

        public AccountRepository(AbatapDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Account> FindAll()
        {
            return _dbContext.Account.ToList();
        }

        public Account GetAccountByNumber(string accountNumber)
        {
            return _dbContext.Account.FirstOrDefault(a => a.AccountNumber == accountNumber);
        }

        public Account Get(int id)
        {
            return _dbContext.Account.FirstOrDefault(x => x.Id == id);
        }

        public void Save(Account entity)
        {
            if (entity.Id > 0)
                _dbContext.Account.Attach(entity);
            else
                _dbContext.Account.Add(entity);
        }

        public void Delete(Account entity)
        {
            _dbContext.Account.Remove(entity);
        }

        public List<Account> GetAccountsForCustomer(int customerId)
        {
            return _dbContext.Account.Where(a => a.CustomerId == customerId).ToList();
        }
    }
}
