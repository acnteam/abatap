﻿using System.Collections.Generic;
using System.Linq;
using Abatap.DataLayer.Model;

namespace Abatap.DataLayer.Database.Repository.Repositories
{
    public class TransferRepository : ITransferRepository
    {
        private readonly AbatapDbContext _dbContext;

        public TransferRepository(AbatapDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Transfer> FindAll()
        {
            return _dbContext.Transfer.ToList();
        }

        public Transfer Get(int id)
        {
            return _dbContext.Transfer.FirstOrDefault(x => x.Id == id);
        }

        public void Save(Transfer entity)
        {
            if (entity.Id > 0)
                _dbContext.Transfer.Attach(entity);
            else
                _dbContext.Transfer.Add(entity);
        }

        public void Delete(Transfer entity)
        {
            _dbContext.Transfer.Remove(entity);
        }
    }
}
