﻿using System.Collections.Generic;
using Abatap.Web.ClientServices;
using Abatap.Services.Contracts.Model;

namespace Abatap.Web.ServiceProxies.Customer
{
    public interface ICustomerServiceProxy : IServiceProxy
    {
        IEnumerable<CustomerDto> GetAllCustomers();

        CustomerDto SaveCustomer(CustomerDto customer);

        void DeleteCustomer(int customerId);
    }
}
