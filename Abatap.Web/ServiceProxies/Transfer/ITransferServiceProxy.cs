﻿using System.Collections.Generic;
using Abatap.Web.ClientServices;
using Abatap.Services.Contracts.Model;

namespace Abatap.Web.ServiceProxies.Transfer
{
    public interface ITransferServiceProxy : IServiceProxy
    {
        IEnumerable<TransferDto> GetTransfersHistory();
        TransferResultDto ExecuteTransfer(TransferRequestDto transferRequest);
    }
}
