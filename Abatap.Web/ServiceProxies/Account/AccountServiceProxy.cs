﻿using System.Collections.Generic;
using System.Configuration;
using Abatap.Web.ClientServices;
using Abatap.Web.ServiceProxies.Customer;
using Abatap.Services.Contracts;
using Abatap.Services.Contracts.Model;

namespace Abatap.Web.ServiceProxies.Account
{
    public class AccountServiceProxy : IAccountServiceProxy
    {
        private readonly IAccountService _service;

        public AccountServiceProxy()
        {
            var serviceFactory = new ServiceFactory<IAccountService>();
            _service = serviceFactory.GetService(ConfigurationManager.AppSettings["AccountServiceAdress"]);
        }

        public IEnumerable<AccountDto> GetAccounts(int customerId)
        {
            return _service.GetAccounts(customerId);
        }

        public List<string> GetAvailableCurrencies()
        {
            return _service.GetAvailableCurrencies();
        }

        public void OpenAccount(int customerId, string currency)
        {
            _service.OpenAccount(customerId, currency);
        }
    }
}
