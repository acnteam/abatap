﻿using System;
using System.Collections.Generic;
using Abatap.Web.ClientServices;
using Abatap.Services.Contracts.Model;

namespace Abatap.Web.ServiceProxies.Account
{
    public interface IAccountServiceProxy : IServiceProxy
    {
        IEnumerable<AccountDto> GetAccounts(int customerId);

        List<string> GetAvailableCurrencies();
    }
}
