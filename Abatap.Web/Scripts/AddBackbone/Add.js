﻿/// <reference path="../jquery-1.10.2.js" />
/// <reference path="../underscore.js" />
/// <reference path="../backbone.js" />

var AddModel = Backbone.Model.extend({
    urlRoot: '/CustomerList/AddBackbone',
    url: '/CustomerList/AddBackbone'
});

var addModel = new AddModel();

var AddView = Backbone.View.extend({
    el: $("#Content"),
    model: addModel,
    events: {
        "click #add_button": "add"
    },
    initialize: function () {
        this.render();
    },

    render: function () {
        var template = _.template($("#addTemplate").html());
        this.$el.html(template);
    },
    add: function () {
        this.model.set({
            Name: this.$el.find('#addFirstName').val(),
            Surname: this.$el.find('#addLastName').val(),
            PersonalId: this.$el.find('#addPesel').val()
        })
        this.model.save({},
            {
                success: function (model, response) {
                    window.location.href = '/CustomerList/Index';
                    console.log('Successfully saved!');
                },
                error: function (model, error) {
                    this.Backbone.$("#Content").html(error.responseText);
                    console.log(model.toJSON());
                    console.log('error.responseText');
                }
            });
    }
})

var addView = new AddView();