﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Abatap.Web.Models
{
    public class CustomerListModel
    {
        public DateTime GeneratedDate { get; set; }
        public List<Customer> CustomerList { get; set; }
    }
}