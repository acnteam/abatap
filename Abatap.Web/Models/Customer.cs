﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Abatap.Web.Models
{
    public class Customer
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string PersonalId { get; set; }
    }
}