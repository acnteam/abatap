﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Abatap.Web.Startup))]
namespace Abatap.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
