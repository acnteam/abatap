﻿using Abatap.Services.Contracts.Model;
using Abatap.Web.Models;
using Abatap.Web.ServiceProxies.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Abatap.Web.Controllers
{
    public class CustomerListController : Controller
    {
        public ActionResult Index()
        {
            CustomerServiceProxy proxy = new CustomerServiceProxy();
            IEnumerable<CustomerDto> customers = proxy.GetAllCustomers();

            CustomerListModel model = new CustomerListModel();
            model.CustomerList = new List<Customer>();
            foreach (var item in customers)
            {
                model.CustomerList.Add(new Customer() { Name = item.Name, Surname = item.Surname });
            }
            model.GeneratedDate = DateTime.Now;
            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(Customer c)
        {
            CustomerServiceProxy proxy = new CustomerServiceProxy();
            CustomerDto customer = new CustomerDto() { Name = c.Name, Surname = c.Surname, PersonalId = c.PersonalId };
            proxy.SaveCustomer(customer);
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult AddBackbone()
        {
            return View();
        }


        [HttpPost]
        public JsonResult AddBackbone(Customer c)
        {
            CustomerServiceProxy proxy = new CustomerServiceProxy();
            CustomerDto customer = new CustomerDto() { Name = c.Name, Surname = c.Surname, PersonalId = c.PersonalId };
            proxy.SaveCustomer(customer);
            return  Json(new HttpResponseMessage(HttpStatusCode.OK));
        }

    }
}