﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abatap.Client.Presenters.Accounts;
using Abatap.Client.Presenters.Customers;
using Abatap.Client.Presenters.Transfers;
using Abatap.Client.ServiceProxies.Account;
using Abatap.Client.ServiceProxies.Customer;
using Abatap.Client.ServiceProxies.Transfer;
using Abatap.Client.Views.Accounts;
using Abatap.Client.Views.Customers;
using Abatap.Client.Views.Transfers;
using Microsoft.Practices.Unity;

namespace Abatap.Client.Infrastructure
{
    public class UnityBootstraper
    {
        public IUnityContainer CreateContainer()
        {
            IUnityContainer container = new UnityContainer();

            container.RegisterType<ICustomerServiceProxy, CustomerServiceProxy>();
            container.RegisterType<ITransferServiceProxy, TransferServiceProxy>();
            container.RegisterType<IAccountServiceProxy, AccountServiceProxy>();

            container.RegisterType<AccountListPresenter>();
            container.RegisterType<OpenAccountPresenter>();
            container.RegisterType<CustomerListPresenter>();
            container.RegisterType<CustomerDetailsPresenter>();
            container.RegisterType<TransferListPresenter>();
            container.RegisterType<ExecuteTransferPresenter>();

            container.RegisterType<IAccountListView, AccountListView>();
            container.RegisterType<IOpenAccountView, OpenAccountView>();

            container.RegisterType<ICustomerListView, CustomerListView>();
            container.RegisterType<ICustomerDetailsView, CustomerDetailsView>();

            container.RegisterType<ITransferListView, TransferListView>();
            container.RegisterType<IExecuteTransferView, ExecuteTransferView>();

            return container;
        }
    }
}
