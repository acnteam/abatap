﻿using System;
using System.Windows.Forms;

namespace Abatap.Client.Infrastructure
{
    internal class WaitCursor : IDisposable
    {
        private readonly Cursor _saved;

        public WaitCursor()
        {
            _saved = Cursor.Current;

            Cursor.Current = Cursors.WaitCursor;
        }

        public void Dispose()
        {
            Cursor.Current = _saved;
        }
    }
}