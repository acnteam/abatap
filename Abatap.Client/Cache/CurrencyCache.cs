﻿using System;
using System.Collections.Generic;
using Abatap.Client.ServiceProxies.Account;

namespace Abatap.Client.Cache
{
    public class CurrencyCache
    {
        private static volatile CurrencyCache _instance;
        private static readonly object SyncRoot = new object();
        private static DateTime _lastRefreshDate = DateTime.MinValue;

        private List<string> _currencies = new List<string>();

        private CurrencyCache()
        {
            RefreshCurrencyList();
        }

        public static CurrencyCache Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null || _lastRefreshDate < DateTime.Now.AddMinutes(-1))
                            _instance = new CurrencyCache();
                    }
                }

                return _instance;
            }
        }

        private void RefreshCurrencyList()
        {
            var accountServiceProxy = new AccountServiceProxy();
            _currencies = accountServiceProxy.GetAvailableCurrencies();
            _lastRefreshDate = DateTime.Now;
        }

        public List<string> GetAvailableCurrenciesCached()
        {
            return _currencies;
        }
    }
}
