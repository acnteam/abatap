﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abatap.Client.Presenters.Accounts;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.Views.Accounts
{
    public interface IAccountListView : IView
    {
        AccountListPresenter Presenter { get; set; }
        IEnumerable<AccountDto> Accounts { set; }
    }
}
