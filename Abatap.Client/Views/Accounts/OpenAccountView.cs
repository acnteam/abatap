﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Abatap.Client.Infrastructure;
using Abatap.Client.Presenters.Accounts;

namespace Abatap.Client.Views.Accounts
{
    public partial class OpenAccountView : Form, IOpenAccountView
    {
        public OpenAccountView()
        {
            InitializeComponent();
        }

        public OpenAccountPresenter Presenter { get; set; }

        public string Currency
        {
            get { return cmbCurrency.SelectedItem.ToString(); }
        }

        public List<string> AvailableCurrencies
        {
            set { cmbCurrency.DataSource = value; }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                Presenter.OpenAccount();
                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}