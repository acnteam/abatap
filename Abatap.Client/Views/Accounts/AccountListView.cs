﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Abatap.Client.Infrastructure;
using Abatap.Client.Presenters.Accounts;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.Views.Accounts
{
    public partial class AccountListView : Form, IAccountListView
    {
        public AccountListPresenter Presenter { get; set; }

        public IEnumerable<AccountDto> Accounts
        {
            set { grdCustomerList.DataSource = value; }
        }

        public AccountListView()
        {
            InitializeComponent();
        }

        private void btnOpenAccount_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                Presenter.OpenNewAccount();
            }
        }

        private void btnExecuteTransfer_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                Presenter.ExecuteTransfer();
            }
        }
    }
}
