﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abatap.Client.Presenters.Accounts;

namespace Abatap.Client.Views.Accounts
{
    public interface IOpenAccountView : IView
    {
        OpenAccountPresenter Presenter { get; set; }
        string Currency { get; }
        List<string> AvailableCurrencies { set; }
    }
}
