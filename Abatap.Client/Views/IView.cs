﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abatap.Client.Presenters;

namespace Abatap.Client.Views
{
    public interface IView : IDisposable
    {
        event EventHandler Load;
        event EventHandler Disposed;
    }
}
