﻿using System;
using System.Windows.Forms;
using Abatap.Client.Infrastructure;
using Abatap.Client.Presenters.Customers;

namespace Abatap.Client.Views.Customers
{
    public partial class CustomerDetailsView : Form, ICustomerDetailsView
    {
        public CustomerDetailsView()
        {
            InitializeComponent();
        }

        public CustomerDetailsPresenter Presenter { get; set; }

        public string CustomerName
        {
            get { return txtName.Text; }
            set { txtName.Text = value; }
        }

        public string Surname
        {
            get { return txtSurname.Text; }
            set { txtSurname.Text = value; }
        }

        public string PersonalId
        {
            get { return txtPersonalId.Text; }
            set { txtPersonalId.Text = value; }
        }

        public string Phone
        {
            get { return txtPhone.Text; }
            set { txtPhone.Text = value; }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (new WaitCursor())
                {
                    Presenter.SaveCustomer();
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Abatap", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}