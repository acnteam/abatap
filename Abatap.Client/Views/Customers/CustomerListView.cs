﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Abatap.Client.Infrastructure;
using Abatap.Client.Presenters.Customers;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.Views.Customers
{
    public partial class CustomerListView : UserControl, ICustomerListView
    {
        public CustomerListPresenter Presenter { get; set; }

        public CustomerListView()
        {
            InitializeComponent();
        }

        public IEnumerable<CustomerDto> Customers
        {
            set { grdCustomerList.DataSource = value; }
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (grdCustomerList.CurrentRow != null)
                    Presenter.ShowCustomer((CustomerDto) grdCustomerList.CurrentRow.DataBoundItem);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (grdCustomerList.CurrentRow != null)
                {
                    var customerToDelete = (CustomerDto) grdCustomerList.CurrentRow.DataBoundItem;
                    Presenter.DeleteCustomer(customerToDelete.Id);
                }
            }
        }

        private void grdCustomerList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            using (new WaitCursor())
            {
                if (grdCustomerList.CurrentRow != null)
                {
                    if (grdCustomerList.CurrentRow != null)
                        Presenter.ShowCustomer((CustomerDto)grdCustomerList.CurrentRow.DataBoundItem);
                }
            }
        }

        private void btnAccounts_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                if (grdCustomerList.CurrentRow != null)
                {
                    if (grdCustomerList.CurrentRow != null)
                        Presenter.ShowAccounts((CustomerDto)grdCustomerList.CurrentRow.DataBoundItem);
                }
            }
        }
    }
}
