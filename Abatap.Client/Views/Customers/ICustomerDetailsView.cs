﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abatap.Client.Presenters.Customers;

namespace Abatap.Client.Views.Customers
{
    public interface ICustomerDetailsView : IView
    {
        CustomerDetailsPresenter Presenter { get; set; }

        string CustomerName { get; set; }

        string Surname { get; set; }

        string PersonalId { get; set; }

        string Phone { get; set; }
    }
}
