﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abatap.Client.Presenters.Customers;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.Views.Customers
{
    public interface ICustomerListView : IView
    {
        CustomerListPresenter Presenter { get; set; }

        IEnumerable<CustomerDto> Customers { set; }
    }
}
