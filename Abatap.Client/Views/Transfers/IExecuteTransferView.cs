﻿using System.Collections.Generic;
using Abatap.Client.Presenters.Transfers;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.Views.Transfers
{
    public interface IExecuteTransferView : IView
    {
        ExecuteTransferPresenter Presenter { get; set; }
        string DestinationAccountNumber { get; set; }
        decimal Amount { get; set; }
        string Currency { get; set; }
    }
}
