﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Abatap.Client.Presenters.Transfers;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.Views.Transfers
{
    public partial class TransferListView : UserControl, ITransferListView
    {
        public TransferListPresenter Presenter { get; set; }

        public TransferListView()
        {
            InitializeComponent();
        }

        public IEnumerable<TransferDto> Transfers
        {
            get { return grdCustomerList.DataSource as IEnumerable<TransferDto>; }
            set { grdCustomerList.DataSource = value; }
        }
    }
}
