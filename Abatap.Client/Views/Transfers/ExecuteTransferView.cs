﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Abatap.Client.Infrastructure;
using Abatap.Client.Presenters.Transfers;

namespace Abatap.Client.Views.Transfers
{
    public partial class ExecuteTransferView : Form, IExecuteTransferView
    {
        public ExecuteTransferPresenter Presenter { get; set; }

        public string DestinationAccountNumber 
        {
            get { return txtDestinationAccountNumber.Text; }
            set { txtDestinationAccountNumber.Text = value; } 
        }

        public decimal Amount
        {
            get { return decimal.Parse(txtAmount.Text); }
            set { txtAmount.Text = value.ToString(); }
        }
        public string Currency
        {
            get { return cmbCurrency.SelectedItem.ToString(); }
            set { cmbCurrency.SelectedItem = value; }
        }

        public ExecuteTransferView()
        {
            InitializeComponent();
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                Presenter.ExecuteTransfer();
                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
