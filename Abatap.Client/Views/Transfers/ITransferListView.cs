﻿using System.Collections.Generic;
using Abatap.Client.Presenters.Transfers;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.Views.Transfers
{
    public interface ITransferListView : IView
    {
        TransferListPresenter Presenter { get; set; }

        IEnumerable<TransferDto> Transfers { get; set; }
    }
}
