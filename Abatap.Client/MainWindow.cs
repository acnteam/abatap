﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Abatap.Client.Infrastructure;
using Abatap.Client.Presenters;
using Abatap.Client.Presenters.Customers;
using Abatap.Client.Presenters.Transfers;
using Abatap.Client.Views;
using Microsoft.Practices.Unity;

namespace Abatap.Client
{
    public partial class MainWindow : Form, IView
    {
        private IUnityContainer _container;

        public MainWindow(IUnityContainer container)
        {
            InitializeComponent();
            _container = container;
        }

        private void toolStripItemCustomerList_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                var customerPresenter = _container.Resolve<CustomerListPresenter>();
                SetInMainPanel(customerPresenter);
            }
        }

        private void toolStripItemAddCustomer_Click(object sender, EventArgs e)
        {
            var presenter = _container.Resolve<CustomerDetailsPresenter>();
            presenter.AddCustomer();

            var customerPresenter = _container.Resolve<CustomerListPresenter>();
            SetInMainPanel(customerPresenter);
        }

        private void toolStripItemTransferList_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                var transferPresenter = _container.Resolve<TransferListPresenter>();
                SetInMainPanel(transferPresenter);
            }
        }

        private void SetInMainPanel(IPresenter presenter)
        {
            var control = presenter.GetView() as Control;
            if (control != null)
            {
                mainPanel.ContentPanel.Controls.Clear();
                mainPanel.ContentPanel.Controls.Add(control);
                control.Dock = DockStyle.Fill;
            }
        }
    }
}
