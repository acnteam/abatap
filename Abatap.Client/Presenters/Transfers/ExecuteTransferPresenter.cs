﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abatap.Client.ServiceProxies.Transfer;
using Abatap.Client.Views.Transfers;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.Presenters.Transfers
{
    public class ExecuteTransferPresenter : PresenterBase<IExecuteTransferView>
    {
        private readonly ITransferServiceProxy _transferServiceProxy;
        public int SourceAccountId { get; set; }
        public string SourceAccountNumber { get; set; }

        public ExecuteTransferPresenter(ITransferServiceProxy transferServiceProxy)
        {
            _transferServiceProxy = transferServiceProxy;
        }

        protected override void InitializeView()
        {
            View.Presenter = this;
        }

        public void ExecuteTransfer()
        {
            var transferRequest = new TransferRequestDto()
            {
                Amount = View.Amount,
                Currency = View.Currency,
                DestinationAccountNumber = View.DestinationAccountNumber,
                SourceAccountId = SourceAccountId
            };
            _transferServiceProxy.ExecuteTransfer(transferRequest);
        }
    }
}
