﻿using System.Collections.Generic;
using Abatap.Client.ServiceProxies.Transfer;
using Abatap.Client.Views.Transfers;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.Presenters.Transfers
{
    public class TransferListPresenter : PresenterBase<ITransferListView>
    {
        private readonly ITransferServiceProxy _transferServiceProxy;

        public TransferListPresenter(ITransferServiceProxy transferServiceProxy)
        {
            _transferServiceProxy = transferServiceProxy;
        }

        protected override void InitializeView()
        {
            View.Presenter = this;

            IEnumerable<TransferDto> transfers = _transferServiceProxy.GetTransfersHistory();
            View.Transfers = transfers;
        }
    }
}