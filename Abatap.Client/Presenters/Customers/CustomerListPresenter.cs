﻿using System.Collections.Generic;
using System.Windows.Forms;
using Abatap.Client.Presenters.Accounts;
using Abatap.Client.ServiceProxies.Customer;
using Abatap.Client.Views.Customers;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.Presenters.Customers
{
    public class CustomerListPresenter : PresenterBase<ICustomerListView>
    {
        private readonly ICustomerServiceProxy _customerServiceProxy;
        private readonly CustomerDetailsPresenter _customerDetailsPresenter;
        private readonly AccountListPresenter _accountListPresenter;

        public CustomerListPresenter(ICustomerServiceProxy customerServiceProxy, 
            CustomerDetailsPresenter customerDetailsPresenter,
            AccountListPresenter accountListPresenter)
        {
            _customerServiceProxy = customerServiceProxy;
            _customerDetailsPresenter = customerDetailsPresenter;
            _accountListPresenter = accountListPresenter;
        }

        protected override void InitializeView()
        {
            View.Presenter = this;
            IEnumerable<CustomerDto> customers = _customerServiceProxy.GetAllCustomers();
            View.Customers = customers;
        }

        public void ShowCustomer(CustomerDto customer)
        {
            if (_customerDetailsPresenter.ShowCustomer(customer) == DialogResult.OK)
                InitializeView();
        }

        public void DeleteCustomer(int customerId)
        {
            _customerServiceProxy.DeleteCustomer(customerId);
            InitializeView();
        }

        public void ShowAccounts(CustomerDto customerId)
        {
            _accountListPresenter.OpenAccountList(customerId.Id);
        }
    }
}