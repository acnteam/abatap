﻿using System.Windows.Forms;
using Abatap.Client.ServiceProxies.Customer;
using Abatap.Client.Views.Customers;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.Presenters.Customers
{
    public class CustomerDetailsPresenter : PresenterBase<ICustomerDetailsView>
    {
        private readonly ICustomerServiceProxy _customerServiceProxy;
        private CustomerDto _customer;

        public CustomerDetailsPresenter(ICustomerServiceProxy customerServiceProxy)
        {
            _customerServiceProxy = customerServiceProxy;
        }

        protected override void InitializeView()
        {
            View.Presenter = this;
        }

        public void AddCustomer()
        {
            var form = View as Form;
            if (form != null)
                form.ShowDialog();
        }

        public DialogResult ShowCustomer(CustomerDto customer)
        {
            _customer = customer;
            View.CustomerName = customer.Name;
            View.PersonalId = customer.PersonalId;
            View.Phone = customer.Phone;
            View.Surname = customer.Surname;

            var form = View as Form;
            if (form != null)
                return form.ShowDialog();
            return DialogResult.Cancel;
        }

        public void SaveCustomer()
        {
            if (_customer == null)
                _customer = new CustomerDto();
            _customer.Name = View.CustomerName;
            _customer.PersonalId = View.PersonalId;
            _customer.Phone = View.Phone;
            _customer.Surname = View.Surname;

            _customer = _customerServiceProxy.SaveCustomer(_customer);
        }
    }
}