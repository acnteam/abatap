﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abatap.Client.Views;

namespace Abatap.Client.Presenters
{
    public interface IPresenter : IDisposable
    {
        IView GetView();
    }
}
