﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abatap.Client.Views;
using Microsoft.Practices.Unity;

namespace Abatap.Client.Presenters
{
    public abstract class PresenterBase<TView> : IPresenter where TView : IView
    {
        private TView _view;
        public bool IsViewLoaded { get; set; }

        [Dependency]
        public TView View
        {
            get { return _view; }
            set
            {
                _view = value;
                OnViewAssigned();
                _view.Load += (s, e) => OnViewLoaded();
            }
        }

        public virtual void OnViewAssigned()
        {

        }

        protected abstract void InitializeView();

        public IView GetView()
        {
            return View;
        }

        protected virtual void OnViewLoaded()
        {
            IsViewLoaded = true;
            InitializeView();
        }

        public virtual void Dispose()
        {

        }
    }
 
}
