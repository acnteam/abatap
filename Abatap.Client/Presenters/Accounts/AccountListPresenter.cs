﻿using System;
using System.Windows.Forms;
using Abatap.Client.Presenters.Transfers;
using Abatap.Client.ServiceProxies.Account;
using Abatap.Client.Views.Accounts;

namespace Abatap.Client.Presenters.Accounts
{
    public class AccountListPresenter : PresenterBase<IAccountListView>
    {
        private int _customerId;
        private readonly IAccountServiceProxy _accountServiceProxy;
        private readonly ExecuteTransferPresenter _executeTransferPresenter;
        private readonly OpenAccountPresenter _openAccountPresenter;

        public AccountListPresenter(IAccountServiceProxy accountServiceProxy,
             ExecuteTransferPresenter executeTransferPresenter,
             OpenAccountPresenter openAccountPresenter)
        {
            _accountServiceProxy = accountServiceProxy;
            _executeTransferPresenter = executeTransferPresenter;
            _openAccountPresenter = openAccountPresenter;
        }

        protected override void InitializeView()
        {
            View.Presenter = this;
            View.Accounts = _accountServiceProxy.GetAccounts(_customerId);
        }

        public void OpenAccountList(int customerId)
        {
            _customerId = customerId;
            _openAccountPresenter.CustomerId = _customerId;

            var form = View as Form;
            if (form != null) form.ShowDialog();
        }

        public void OpenNewAccount()
        {
            _openAccountPresenter.ShowOpenAccountWindow(_customerId);
            InitializeView();
        }

        public void ExecuteTransfer()
        {
            var form = _executeTransferPresenter.View as Form;
                if (form != null) form.ShowDialog();
        }
    }
}
