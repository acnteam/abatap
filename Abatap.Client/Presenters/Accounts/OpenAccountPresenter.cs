﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Abatap.Client.Cache;
using Abatap.Client.ServiceProxies.Account;
using Abatap.Client.Views.Accounts;

namespace Abatap.Client.Presenters.Accounts
{
    public class OpenAccountPresenter : PresenterBase<IOpenAccountView>
    {
        private readonly AccountServiceProxy _accountServiceProxy;

        public int CustomerId { get; set; }

        public OpenAccountPresenter(AccountServiceProxy accountServiceProxy)
        {
            _accountServiceProxy = accountServiceProxy;
        }

        protected override void InitializeView()
        {
            View.Presenter = this;
        }

        public void OpenAccount()
        {
            _accountServiceProxy.OpenAccount(CustomerId, View.Currency);
        }

        public void ShowOpenAccountWindow(int customerId)
        {
            View.AvailableCurrencies = CurrencyCache.Instance.GetAvailableCurrenciesCached();
            var form = View as Form;
            if (form != null) form.ShowDialog();
        }
    }
}
