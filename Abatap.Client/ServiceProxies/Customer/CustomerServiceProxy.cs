﻿using System.Collections.Generic;
using System.Configuration;
using Abatap.Client.ClientServices;
using Abatap.Services.Contracts;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.ServiceProxies.Customer
{
    public class CustomerServiceProxy : ICustomerServiceProxy
    {
        private readonly ICustomerService _service;

        public CustomerServiceProxy()
        {
            var serviceFactory = new ServiceFactory<ICustomerService>();
            _service = serviceFactory.GetService(ConfigurationManager.AppSettings["CustomerServiceAdress"]);
        }

        public IEnumerable<CustomerDto> GetAllCustomers()
        {
            IEnumerable<CustomerDto> customers = _service.GetAllCustomers();
            return customers;
        }

        public CustomerDto SaveCustomer(CustomerDto customer)
        {
            return _service.SaveCustomer(customer);
        }

        public void DeleteCustomer(int customerId)
        {
            _service.DeleteCustomer(customerId);
        }
    }
}
