﻿using System.Collections.Generic;
using Abatap.Client.ClientServices;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.ServiceProxies.Customer
{
    public interface ICustomerServiceProxy : IServiceProxy
    {
        IEnumerable<CustomerDto> GetAllCustomers();

        CustomerDto SaveCustomer(CustomerDto customer);

        void DeleteCustomer(int customerId);
    }
}
