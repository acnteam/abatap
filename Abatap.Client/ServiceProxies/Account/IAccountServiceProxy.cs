﻿using System;
using System.Collections.Generic;
using Abatap.Client.ClientServices;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.ServiceProxies.Account
{
    public interface IAccountServiceProxy : IServiceProxy
    {
        IEnumerable<AccountDto> GetAccounts(int customerId);

        List<string> GetAvailableCurrencies();
    }
}
