﻿using System.Collections.Generic;
using System.Configuration;
using Abatap.Client.ClientServices;
using Abatap.Services.Contracts;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.ServiceProxies.Transfer
{
    public class TransferServiceProxy : ITransferServiceProxy
    {
        private readonly ITransferService _service;

        public TransferServiceProxy()
        {
            var serviceFactory = new ServiceFactory<ITransferService>();
            _service = serviceFactory.GetService(ConfigurationManager.AppSettings["TransferServiceAdress"]);
        }

        public IEnumerable<TransferDto> GetTransfersHistory()
        {
            return _service.GetTransfersHistory();
        }

        public TransferResultDto ExecuteTransfer(TransferRequestDto transferRequest)
        {
            return _service.ExecuteTransfer(transferRequest);
        }
    }
}
