﻿using System.Collections.Generic;
using Abatap.Client.ClientServices;
using Abatap.Services.Contracts.Model;

namespace Abatap.Client.ServiceProxies.Transfer
{
    public interface ITransferServiceProxy : IServiceProxy
    {
        IEnumerable<TransferDto> GetTransfersHistory();
        TransferResultDto ExecuteTransfer(TransferRequestDto transferRequest);
    }
}
