﻿namespace Abatap.Client
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.mainPanel = new System.Windows.Forms.ToolStripContainer();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.klienciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripItemCustomerList = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripItemAddCustomer = new System.Windows.Forms.ToolStripMenuItem();
            this.przelewyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripItemTransferList = new System.Windows.Forms.ToolStripMenuItem();
            this.mainPanel.TopToolStripPanel.SuspendLayout();
            this.mainPanel.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 331);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(711, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // mainPanel
            // 
            // 
            // mainPanel.ContentPanel
            // 
            this.mainPanel.ContentPanel.Size = new System.Drawing.Size(711, 307);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(711, 331);
            this.mainPanel.TabIndex = 1;
            this.mainPanel.Text = "toolStripContainer1";
            // 
            // mainPanel.TopToolStripPanel
            // 
            this.mainPanel.TopToolStripPanel.Controls.Add(this.menuStrip1);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.klienciToolStripMenuItem,
            this.przelewyToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(711, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // klienciToolStripMenuItem
            // 
            this.klienciToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripItemCustomerList,
            this.toolStripItemAddCustomer});
            this.klienciToolStripMenuItem.Name = "klienciToolStripMenuItem";
            this.klienciToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.klienciToolStripMenuItem.Text = "Klienci";
            // 
            // toolStripItemCustomerList
            // 
            this.toolStripItemCustomerList.Name = "toolStripItemCustomerList";
            this.toolStripItemCustomerList.Size = new System.Drawing.Size(147, 22);
            this.toolStripItemCustomerList.Text = "Lista Klientów";
            this.toolStripItemCustomerList.Click += new System.EventHandler(this.toolStripItemCustomerList_Click);
            // 
            // toolStripItemAddCustomer
            // 
            this.toolStripItemAddCustomer.Name = "toolStripItemAddCustomer";
            this.toolStripItemAddCustomer.Size = new System.Drawing.Size(147, 22);
            this.toolStripItemAddCustomer.Text = "Dodaj Klienta";
            this.toolStripItemAddCustomer.Click += new System.EventHandler(this.toolStripItemAddCustomer_Click);
            // 
            // przelewyToolStripMenuItem
            // 
            this.przelewyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripItemTransferList});
            this.przelewyToolStripMenuItem.Name = "przelewyToolStripMenuItem";
            this.przelewyToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.przelewyToolStripMenuItem.Text = "Przelewy";
            // 
            // toolStripItemTransferList
            // 
            this.toolStripItemTransferList.Name = "toolStripItemTransferList";
            this.toolStripItemTransferList.Size = new System.Drawing.Size(157, 22);
            this.toolStripItemTransferList.Text = "Lista Przelewów";
            this.toolStripItemTransferList.Click += new System.EventHandler(this.toolStripItemTransferList_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 353);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.statusStrip1);
            this.Name = "MainWindow";
            this.Text = "Abatap - Terminal Finansowy";
            this.mainPanel.TopToolStripPanel.ResumeLayout(false);
            this.mainPanel.TopToolStripPanel.PerformLayout();
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripContainer mainPanel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem klienciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem przelewyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripItemCustomerList;
        private System.Windows.Forms.ToolStripMenuItem toolStripItemAddCustomer;
        private System.Windows.Forms.ToolStripMenuItem toolStripItemTransferList;
    }
}

