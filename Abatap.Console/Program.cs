﻿using System.Collections.Generic;
using System.Configuration;
using Abatap.Services.Contracts;
using Abatap.Services.Contracts.Model;

namespace Abatap.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceFactory<ICustomerService> cusProxy = new ServiceFactory<ICustomerService>();
            ICustomerService cusService = cusProxy.GetService(ConfigurationManager.AppSettings["CustomerServiceAdress"]);
            IEnumerable<CustomerDto> customers = cusService.GetAllCustomers();
        }
    }
}
