﻿Abatap.Services.SelfHost application is used to host WCF Services from *.exe program.
It is unlike WebHost that uses IIS to host services.

SelfHost can be used as a startup project for server side of Abatap application.
This is why it has reference to AbatapDatabase.mdf file. By default this file is marked
as copy-if-newer. So as long as there are no changes in source database project all changes
in mdf file are reserved when program is debugged multiple times.

SelfHost.exe is used as a startup project so App.config file should be used to configure
all run-time parametrs of the application (bindings, connection strings etc.)