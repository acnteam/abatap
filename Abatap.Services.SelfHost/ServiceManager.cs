﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Abatap.Services.SelfHost
{

    public class ServiceManager {
        readonly List<ServiceHost> serviceHosts = new List<ServiceHost>();
        public readonly string baseaddr = "http://localhost:49600/";
  
 
    public void CloseAll() {  
        foreach (ServiceHost serviceHost in serviceHosts)  
            serviceHost.Close();  
    }  
  
    public void OpenHost<T,TIt>() {  
        Type type = typeof(T);
        Uri adr = new Uri(baseaddr + type.Name);
        ServiceHost serviceHost = new ServiceHost(type, adr);
        serviceHost.AddServiceEndpoint(typeof (TIt), new BasicHttpBinding(), adr);
        
        serviceHost.Open();
        Console.WriteLine("The service is open at {0}", adr);
        
        serviceHosts.Add(serviceHost);  
    }  
}  
}
