﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using Abatap.AppSrv.Business.Services;
using Abatap.Services.Contracts;

namespace Abatap.Services.SelfHost
{
    class Program
    {
        static void Main(string[] args)
        {
            
            ServiceManager srvMgr = new ServiceManager();

            srvMgr.OpenHost<TransferService,ITransferService>();
            srvMgr.OpenHost<AccountService,IAccountService>();
            srvMgr.OpenHost<CustomerService,ICustomerService>();
            
            Console.WriteLine("The service is ready at {0}", srvMgr.baseaddr);
            Console.WriteLine("Press <Enter>; to stop the service.");
            Console.ReadLine();


            // Close the ServiceHost.
            srvMgr.CloseAll();
            }
       }
}

